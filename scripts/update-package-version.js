const { readFileSync, writeFileSync } = require('fs');
const { resolve, join } = require('path');

const ROOT = resolve(join(__dirname, '../'));
const encoding = 'UTF-8';
const version = require(join(ROOT, 'package.json')).version;

const VERSION_PATH = resolve(join(ROOT, 'hestia_earth/models', 'version.py'));
let content = readFileSync(VERSION_PATH, encoding);
content = content.replace(/VERSION\s=\s\'[\d\-a-z\.]+\'/, `VERSION = '${version}'`);
writeFileSync(VERSION_PATH, content, encoding);

const LAYER_REQUIREMENTS_PATH = resolve(join(ROOT, 'layer', 'requirements.txt'));
content = readFileSync(LAYER_REQUIREMENTS_PATH, encoding);
content = content.replace(/hestia_earth.models>=[\d\-a-z\.]+/, `hestia_earth.models>=${version}`);
writeFileSync(LAYER_REQUIREMENTS_PATH, content, encoding);
