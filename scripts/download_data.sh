#!/bin/bash

# Script to download all the data necessary data prior to running the engine.

API_URL=$1
BASE_DIR="${2:-"./"}hestia_earth/models/data"

DEST="${BASE_DIR}/properties"
mkdir -p $DEST

echo "Downloading Terms..."

curl -s -X POST $API_URL/search \
  -H "Content-Type: application/json" \
  -d @scripts/find-term-water.json | python3 -mjson.tool | grep '@id' | sed 's/ //g' | sed 's/"@id"://g' | sed 's/"//g' | sed 's/,//g' \
  > $DEST/irrigations.txt

curl -s -X POST $API_URL/search \
  -H "Content-Type: application/json" \
  -d @scripts/find-term-liquid-fuel.json | python3 -mjson.tool | grep '@id' | sed 's/ //g' | sed 's/"@id"://g' | sed 's/"//g' | sed 's/,//g' \
  > $DEST/liquid_fuel.txt
