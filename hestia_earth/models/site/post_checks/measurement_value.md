# Measurement Value

This model calculates the `value` of the [Measurement](https://hestia.earth/schema/Measurement) by taking an average from the `min` and `max` values.

## Returns

- a list of [Measurement](https://hestia.earth/schema/Measurement) with added [measurement.value](https://hestia.earth/schema/Measurement#value)

## Requirements

A [Site](https://hestia.earth/schema/Site) with:
- a list of [measurements](https://hestia.earth/schema/Site#measurements)

## Usage

1. Install the library: `pip install hestia_earth.models`
```python
from hestia_earth.models.site.post_checks.measurement_value import run

measurements = run(site)
print(measurements)
```

## Implementation Status

- `Implemented`
- [Tested](../../../tests/site_post_models/test_measurement_value.py)
