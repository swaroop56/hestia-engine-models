# Soil Total Nitrogen Content

Calculates soil total Nitrogen concentration for the `Site`.
Based on data from [Batjes (2015) ISRIC report 2015/01 World Soil Information](https://www.researchgate.net/publication/282185041_World_soil_property_estimates_for_broad-_scale_modelling_WISE30sec).
If `Site` location is a spatial point (`latitude` and `longitude`), this model returns the total Nitrogen concentration of the soil at the point location.
If `Site` location is a spatial boundary (either a custom [boundary](https://www.hestia.earth/schema/Site#boundary), or a boundary automatically derived from the GADM [region](https://www.hestia.earth/schema/Site#region)), this model returns the mean total Nitrogen concentration of the soils within the boundary.

## Returns

- [Measurement](https://hestia.earth/schema/Measurement) with fields:
  - [term](https://hestia.earth/schema/Measurement#term) with [soilTotalNitrogenContent](https://hestia.earth/term/soilTotalNitrogenContent)
  - [value](https://hestia.earth/schema/Measurement#value)
  - [statsDefinition](https://hestia.earth/schema/Measurement#statsDefinition) with `spatial`
  - [depthUpper](https://hestia.earth/schema/Measurement#depthUpper)
  - [depthLower](https://hestia.earth/schema/Measurement#depthLower)

## Requirements

* A [Site](https://hestia.earth/schema/Site) with spatial information in one of the following formats:
  - Spatial point: [latitude](https://www.hestia.earth/schema/Site#latitude) and [longitude](https://www.hestia.earth/schema/Site#longitude)
  - Spatial boundary: [boundary](https://www.hestia.earth/schema/Site#boundary)
  - If no spatial data are provided, the boundary for the GADM [region](https://www.hestia.earth/schema/Site#region) is used

## Usage

1. Install the library: `pip install hestia_earth.models`
```python
from hestia_earth.models.spatial import run

measurement = run('soilTotalNitrogenContent', site)
print(measurement['value'])
```

## Implementation Status

- `Implemented`
- [Tested](../../../tests/spatial/test_soilTotalNitrogenContent.py)
