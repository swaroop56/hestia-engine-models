# Drainage Class

Drainage class is a six class categorical variable extracted from the [Harmonized World Soil Database, version 1.2 (FAO, 2012)](http://www.fao.org/3/aq361e/aq361e.pdf).
This model calculates the [drainage class](https://www.hestia.earth/term/drainageClass) of the soils located at `Site`.
If `Site` location is a spatial point (`latitude` and `longitude`), this model returns the drainage class at the given point location.
If `Site` location is a spatial boundary (either a custom [boundary](https://www.hestia.earth/schema/Site#boundary), or a boundary automatically derived from the GADM [region](https://www.hestia.earth/schema/Site#region)), the dominant (modal) drainage class is returned.

## Returns

- [Measurement](https://hestia.earth/schema/Measurement) with fields:
  - [term](https://hestia.earth/schema/Measurement#term) with [drainageClass](https://hestia.earth/term/drainageClass)
  - [value](https://hestia.earth/schema/Measurement#value)
  - [statsDefinition](https://hestia.earth/schema/Measurement#statsDefinition) with `spatial`

## Requirements

* A [Site](https://hestia.earth/schema/Site) have spatial information in one of the following formats:
  - Spatial point: [latitude](https://www.hestia.earth/schema/Site#latitude) and [longitude](https://www.hestia.earth/schema/Site#longitude)
  - Spatial boundary: [boundary](https://www.hestia.earth/schema/Site#boundary)
  - If no spatial data are provided, the boundary for the GADM [region](https://www.hestia.earth/schema/Site#region) is used

## Usage

1. Install the library: `pip install hestia_earth.models`
```python
from hestia_earth.models.spatial import run

measurement = run('drainageClass', site)
print(measurement['value'])
```

## Implementation Status

- `Implemented`
- [Tested](../../../tests/spatial/test_drainageClass.py)
