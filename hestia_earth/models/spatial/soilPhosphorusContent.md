# Soil Phosphorus Content

This model calculates the Phosphorus concentration of the soil at the `Site`.
The data are derived from Phosphorus area density to a depth of 50cm, see [Scherer & Pfister, (2015) Int. J. Life Cycle Assess. 20, 785–795](https://link.springer.com/article/10.1007%2Fs11367-015-0880-0).
If `Site` location is a spatial point (`latitude` and `longitude`), this model returns the Phosphorus concentration of the soil at the point location.
If `Site` location is a spatial boundary (either a custom [boundary](https://www.hestia.earth/schema/Site#boundary), or a boundary automatically derived from the GADM [region](https://www.hestia.earth/schema/Site#region)), this model returns the mean Phosphorus concentration of the soil within the boundary.

## Returns

- [Measurement](https://hestia.earth/schema/Measurement) with fields:
  - [term](https://hestia.earth/schema/Measurement#term) with [soilPhosphorusContent](https://hestia.earth/term/soilPhosphorusContent)
  - [value](https://hestia.earth/schema/Measurement#value)
  - [statsDefinition](https://hestia.earth/schema/Measurement#statsDefinition) with `spatial`
  - [depthUpper](https://hestia.earth/schema/Measurement#depthUpper)
  - [depthLower](https://hestia.earth/schema/Measurement#depthLower)

## Requirements

* A [Site](https://hestia.earth/schema/Site) with spatial information in one of the following formats:
  - Spatial point: [latitude](https://www.hestia.earth/schema/Site#latitude) and [longitude](https://www.hestia.earth/schema/Site#longitude)
  - Spatial boundary: [boundary](https://www.hestia.earth/schema/Site#boundary)
  - If no spatial data are provided, the boundary for the GADM [region](https://www.hestia.earth/schema/Site#region) is used

## Usage

1. Install the library: `pip install hestia_earth.models`
```python
from hestia_earth.models.spatial import run

measurement = run('soilPhosphorusContent', site)
print(measurement['value'])
```

## Implementation Status

- `Implemented`
- [Tested](../../../tests/spatial/test_soilPhosphorusContent.py)
