# Aware Water Basin ID

This model calculates the the [AWARE](https://wulca-waterlca.org/) water basin identifier.

## Returns

- The AWARE water basin identifier as a `string`.

## Requirements

* A [Site](https://hestia.earth/schema/Site) with spatial information in one of the following formats:
  - Spatial point: [latitude](https://www.hestia.earth/schema/Site#latitude) and [longitude](https://www.hestia.earth/schema/Site#longitude)
  - Spatial boundary: [boundary](https://www.hestia.earth/schema/Site#boundary)
  - If no spatial data are provided, the boundary for the GADM [region](https://www.hestia.earth/schema/Site#region) is used

## Usage

1. Install the library: `pip install hestia_earth.models`
```python
from hestia_earth.models.spatial import run

measurement = run('aware', site)
print(measurement['value'])
```

## Implementation Status

- `Implemented`
- [Tested](../../../tests/spatial/test_aware.py)
