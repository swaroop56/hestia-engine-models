# Temperature Annual

Calculates the mean annual temperature in the final year of the `Cycle` for the `Site`.
Based on data from the [Copernicus programme, ERA5 produced by ECMWF/Copernicus Climate Change Service](https://cds.climate.copernicus.eu/cdsapp#!/dataset/reanalysis-era5-single-levels?tab=overview)
*NOTE* This model is in development, due to be updated during the week starting 22nd March.

## Returns

- [Measurement](https://hestia.earth/schema/Measurement) with fields:
  - [term](https://hestia.earth/schema/Measurement#term) with [temperatureAnnual](https://hestia.earth/term/temperatureAnnual)
  - [value](https://hestia.earth/schema/Measurement#value)
  - [statsDefinition](https://hestia.earth/schema/Measurement#statsDefinition) with `spatial`
  - [startDate](https://hestia.earth/schema/Measurement#startDate)
  - [endDate](https://hestia.earth/schema/Measurement#endDate)

## Requirements

* A [Site](https://hestia.earth/schema/Site) with spatial information in one of the following formats:
  - Spatial point: [latitude](https://www.hestia.earth/schema/Site#latitude) and [longitude](https://www.hestia.earth/schema/Site#longitude)
  - Spatial boundary: [boundary](https://www.hestia.earth/schema/Site#boundary)
  - If no spatial data are provided, the boundary for the GADM [region](https://www.hestia.earth/schema/Site#region) is used
* Must be associated with at least 1 `Cycle` that has an [endDate](https://www.hestia.earth/schema/Cycle#endDate) after `1979`

## Usage

1. Install the library: `pip install hestia_earth.models`
```python
from hestia_earth.models.spatial import run

measurement = run('temperature_annual', site)
print(measurement['value'])
```

## Implementation Status

- `Implemented`
- [Tested](../../../tests/spatial/test_temperature_annual.py)
