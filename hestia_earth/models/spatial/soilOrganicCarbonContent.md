# Soil Organic Carbon

Calculates the soil organic carbon content (%) for the `Site`.
Based on data from the [Harmonized World Soil Database, version 1.2 (FAO, 2012)](http://www.fao.org/3/aq361e/aq361e.pdf).
If `Site` location is a spatial point (`latitude` and `longitude`), this model returns the organic carbon content of the soil at the point location.
If `Site` location is a spatial boundary (either a custom [boundary](https://www.hestia.earth/schema/Site#boundary), or a boundary automatically derived from the GADM [region](https://www.hestia.earth/schema/Site#region)), this model returns the mean organic carbon content of the soils within the boundary.

## Returns

- [Measurement](https://hestia.earth/schema/Measurement) with fields:
  - [term](https://hestia.earth/schema/Measurement#term) with [soilOrganicCarbonContent](https://hestia.earth/term/soilOrganicCarbonContent)
  - [value](https://hestia.earth/schema/Measurement#value)
  - [statsDefinition](https://hestia.earth/schema/Measurement#statsDefinition) with `spatial`
  - [depthUpper](https://hestia.earth/schema/Measurement#depthUpper)
  - [depthLower](https://hestia.earth/schema/Measurement#depthLower)

## Requirements

* A [Site](https://hestia.earth/schema/Site) with spatial information in one of the following formats:
  - Spatial point: [latitude](https://www.hestia.earth/schema/Site#latitude) and [longitude](https://www.hestia.earth/schema/Site#longitude)
  - Spatial boundary: [boundary](https://www.hestia.earth/schema/Site#boundary)
  - If no spatial data are provided, the boundary for the GADM [region](https://www.hestia.earth/schema/Site#region) is used

## Usage

1. Install the library: `pip install hestia_earth.models`
```python
from hestia_earth.models.spatial import run

measurement = run('soilOrganicCarbonContent', site)
print(measurement['value'])
```

## Implementation Status

- `Implemented`
- [Tested](../../../tests/spatial/test_soilOrganicCarbonContent.py)
