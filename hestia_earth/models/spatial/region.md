# Region

This model calculates the geogrpahic region of the `Site`. The model calculates the finest scale GADM region possible, moving from gadm level 5 (for example, a village) to GADM level 0 (Country).

## Returns

- [Term](https://hestia.earth/schema/Term) of [termType](https://hestia.earth/schema/Term#termType) = [region](https://hestia.earth/glossary?termType=region)

## Requirements

* A [Site](https://hestia.earth/schema/Site) with spatial information in one of the following formats.
  - Spatial point: [latitude](https://www.hestia.earth/schema/Site#latitude) and [longitude](https://www.hestia.earth/schema/Site#longitude)
  - Spatial boundary: [boundary](https://www.hestia.earth/schema/Site#boundary)

## Usage

1. Install the library: `pip install hestia_earth.models`
```python
from hestia_earth.models.spatial import run

measurement = run('region', site)
print(measurement['value'])
```

## Implementation Status

- `Implemented`
- [Tested](../../../tests/spatial/test_region.py)
