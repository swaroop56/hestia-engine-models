from hestia_earth.schema import MeasurementStatsDefinition

from hestia_earth.models.log import logger
from hestia_earth.models.utils.measurement import _new_measurement
from .utils import download, has_geospatial_data, _site_gadm_id

TERM_ID = 'soilPh'
BIBLIO_TITLE = 'The harmonized world soil database. verson 1.0'


def _measurement(value: float):
    logger.info('term=%s, value=%s', TERM_ID, value)
    measurement = _new_measurement(TERM_ID, BIBLIO_TITLE)
    measurement['value'] = [value]
    measurement['depthUpper'] = 0
    measurement['depthLower'] = 30
    measurement['statsDefinition'] = MeasurementStatsDefinition.SPATIAL.value
    return measurement


def _run(site: dict):
    field = 'first'
    value = download(collection='users/hestiaplatform/T_PH_H2O',
                     ee_type='raster',
                     latitude=site.get('latitude'),
                     longitude=site.get('longitude'),
                     gadm_id=_site_gadm_id(site),
                     boundary=site.get('boundary'),
                     fields=field
                     ).get(field, None)

    return None if value is None else _measurement(round(value, 1))


def _should_run(site: dict):
    should_run = has_geospatial_data(site)
    logger.info('term=%s, should_run=%s', TERM_ID, should_run)
    return should_run


def run(site: dict): return _run(site) if _should_run(site) else None
