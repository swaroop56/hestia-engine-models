#!/bin/sh
API_URL="https://api-staging.hestia.earth"

docker build --progress=plain \
  -t hestia-engine-models:test \
  -f tests/Dockerfile \
  --build-arg API_URL=$API_URL \
  .
docker run --rm \
  --env-file .env \
  -v ${PWD}/coverage:/app/coverage \
  -v ${PWD}/hestia_earth:/app/hestia_earth \
  -v ${PWD}/tests:/app/tests \
  hestia-engine-models:test
